<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-tabel', 'IndexController@data_tabel');


//CRUD

//Create
Route::get('/cast/create', 'CastController@create'); //Route Menuju Form Create
Route::post('/cast', 'CastController@store'); //Route untuk Menyimpan Data ke Database

//Read
Route::get('/cast', 'CastController@index'); //Route Menampilkan Data
Route::get('/cast/{cast_id}', 'CastController@show'); //Route Detail Data

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route Edit Data
Route::put('/cast/{cast_id}', 'CastController@update'); //Route untuk Update Data

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route Menghapus Data