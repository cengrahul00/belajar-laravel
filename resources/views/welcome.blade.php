@extends('layout.master')

@section('titel')
    Beranda
@endsection

@section('judul')
    Halaman Utama
@endsection

@section('konten')
<h1>SELAMAT DATANG! {{$nama1}} {{$nama2}}</h1>
<h3>Terima Kasih Telah Bergabung di Website Kami. Media Belajar Kita Bersama!</h3>
@endsection