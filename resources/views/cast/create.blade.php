@extends('layout.master')

@section('titel')
    Tambah Data
@endsection

@section('judul')
    Tambah Data
@endsection

@section('konten')
<form action="/cast" method="POST">
    @csrf
    <div class="mb-3">
      <label>Nama Cast</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="mb-3">
      <label>Umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="mb-3">
        <label>Bio</label>
      <textarea name="bio" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection