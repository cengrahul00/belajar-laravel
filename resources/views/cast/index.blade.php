@extends('layout.master')

@section('titel')
    List Data
@endsection

@section('judul')
    List Data
@endsection

@section('konten')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Cast</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $cast)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$cast ->nama}}</td>
                <td>{{$cast ->umur}}</td>
                <td>{{$cast ->bio}}</td>
                <td>
                    <form action="/cast/{{$cast->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ditemukan!</h1>
        @endforelse
    </tbody>
  </table>
@endsection