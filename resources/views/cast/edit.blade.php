@extends('layout.master')

@section('titel')
    Edit Data
@endsection

@section('judul')
    Edit Data
@endsection

@section('konten')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="mb-3">
      <label>Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="mb-3">
        <label>Bio</label>
      <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection