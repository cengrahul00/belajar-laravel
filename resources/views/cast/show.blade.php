@extends('layout.master')

@section('titel')
    Detail Data
@endsection

@section('judul')
    Detail Data
@endsection

@section('konten')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>
@endsection