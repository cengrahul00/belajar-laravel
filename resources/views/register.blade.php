@extends('layout.master')

@section('titel')
    Registrasi
@endsection

@section('judul')
    Form Registrasi
@endsection

@section('konten')
<h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name :</label>
        <br><br>
        <input type="text" name="first_name">
        <br><br>
        <label>Last Name :</label>
        <br><br>
        <input type="text" name="last_name">
        <br><br>
        <label>Gender</label>
        <br><br>
        <input type="radio" name="gender">Male
        <br>
        <input type="radio" name="gender">Female
        <br><br>
        <label>Nationality</label>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapore">Singapore</option>
            <option value="thailand">Thailand</option>
            <option value="vietnam">Vietnam</option>
        </select>
        <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="kirim">
    </form>
    
@endsection
